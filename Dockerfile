# Stage for compiling nginx
FROM alpine:3.12 AS compiler

ARG NGINX_VERSION=1.18.0
ARG ZLIB_VERSION=1.2.11

ADD http://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz /tmp/
ADD https://zlib.net/zlib-${ZLIB_VERSION}.tar.gz /tmp/

RUN apk update && \
    apk add g++ make pcre-dev openssl-dev && \ 
    tar xfz /tmp/nginx*.tar.gz -C /opt/ && \
    tar xfz /tmp/zlib*.tar.gz -C /opt/ && \
    cd /opt/nginx* && \
    ls -l && \
    ./configure \
        --prefix=/usr/local/nginx \
        --sbin-path=/usr/local/nginx/nginx \
        --conf-path=/usr/local/nginx/nginx.conf \
        --pid-path=/usr/local/nginx/nginx.pid \
        --with-http_ssl_module \
        --with-pcre \
        --with-http_stub_status_module \
        --with-zlib=/opt/zlib* && \
    make install && \
    ls -l /usr/local/nginx/

# Main stage. Includes only compiled files, for
# keeping image small
FROM alpine:3.12

RUN apk update && \
    apk add pcre openssl 

COPY --from=compiler /usr/local/nginx/ /usr/local/nginx/
COPY default-conf/ /usr/local/nginx/

EXPOSE 80 443

CMD [ "/usr/local/nginx/nginx", "-g", "daemon off;" ]