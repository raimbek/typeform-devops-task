# Typeform Nginx docker image

Image usage example:

```yaml
version: '3'
  nginx:
    image: 'registry.gitlab.com/raimbek/typeform-devops-task:master'
    ports:
      - 8080:80
    volumes:
      - ./path/to/nginx.conf:/usr/local/nginx/nginx.conf
      - ./path/to/conf.d:/usr/local/nginx/conf.d
```

Used documentation:

- Nginx compiling from source: http://nginx.org/en/docs/configure.html
- Stub status module: http://nginx.org/en/docs/http/ngx_http_stub_status_module.html
- Gitlab docker build: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html


## Monitoring task for junior engineer

Task: develop prometheus exporter for `typeform-nginx` image

Description:

`typeform-nginx` docker image provides `/monitoring` endpoint,
which serves nginx status. Example response:

```
Active connections: 291 
server accepts handled requests
 16630948 16630948 31070465 
Reading: 6 Writing: 179 Waiting: 106
```

Response description you can find [here](http://nginx.org/en/docs/http/ngx_http_stub_status_module.html).

Develop small http server which will provide the nginx metrics to
prometheus, in the other words develop prometheus exporter for 
`typeform-nginx` image. Your script must use `/monitoring` endpoint and
transform it to prometheus syntax. For example, above nginx status
could be transformed to the next response:

```
nginx_active_connections{label="total"} 291
nginx_active_connections{label="reading"} 6
nginx_active_connections{label="writing"} 179
nginx_active_connections{label="waiting"} 106
nginx_requests{label="accepts"} 16630948
nginx_requests{label="handled"} 16630948
nginx_requests{label="total"} 31070465
```

Please follow next recommendations from prometheus:

- https://prometheus.io/docs/instrumenting/writing_exporters/
- https://prometheus.io/docs/practices/naming/

Script must accept monitoring endpoint as parameter. Example of usage:

```
./typeform-nginx-exporter --monitoring-url http://localhost:8080/monitoring
```